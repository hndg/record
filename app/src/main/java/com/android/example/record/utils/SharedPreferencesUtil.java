package com.android.example.record.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferencesUtil {

    /**
     * 存储警号
     */
    public static final String SIREN = "siren";

    /**
     * 存储地址
     */
    public static final String UPLOAD_URI = "upload_uri";

    /**
     * 协议, 默认 为 http, 1 为 https
     */
    public static final String HTTP = "http://";

    public static String getParam(Context context, String key, Object defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(key.replace("://", ""), Context.MODE_PRIVATE);

        return preferences.getString(key, (String) defaultValue);
    }

    public static void setParam(Context context, String key, Object defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(key.replace("://", ""), Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(key, (String) defaultValue);
        editor.apply();
    }

    public static void removeParam(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.remove(key).apply();
    }
}
