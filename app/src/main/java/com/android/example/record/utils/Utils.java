package com.android.example.record.utils;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.baidu.mapapi.model.LatLng;

import java.io.File;

public class Utils {
    public AppCompatActivity activity;
    // "http://192.168.43.179:2580/XSendfile/IllegalCapture/index.php"
    public static String PROTOCOL = "http://";
    public static String UPLOAD_PICTURE_URI = "localhost:88/Picture";

    public static String getUploadPictureUri(AppCompatActivity activity) {

        return SharedPreferencesUtil.getParam(activity, SharedPreferencesUtil.HTTP, PROTOCOL) +
                SharedPreferencesUtil.getParam(activity, SharedPreferencesUtil.UPLOAD_URI, UPLOAD_PICTURE_URI);
    }

    public static boolean isLatLngEqual(LatLng latLng0, LatLng latLng1) {
        return latLng0.latitude != latLng1.latitude
                || latLng0.longitude != latLng1.longitude;
    }

    public static boolean renameFile(String oldFileName, String NewFileName) {
        File oldFile = new File(oldFileName);
        File newFile = new File(NewFileName);
        if (oldFile.exists()) {
            return oldFile.renameTo(newFile);
        }

        return false;
    }

    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return file.delete();
        }

        return false;
    }

    /**
     * 获取文件大小，单位KB
     * @param path 文件路径
     * @return 文件大小变成字符串
     */
    public static String getFileSize(String path) {
        File file = new File(path);
        if (file.exists()) {
            return String.valueOf(file.length() / 1024);
        }
        return "";
    }

    public static void closeKeyboard(AppCompatActivity activity) {
        InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (manager.isActive() && activity.getCurrentFocus() != null) {
            if (activity.getCurrentFocus().getWindowToken() != null) {
                manager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static String getImgPath(Context context) {
        return context.getFilesDir().getPath() + "/picture/";
    }

    public static String getRecordPath(Context context) {
        return context.getFilesDir().getPath() + "/record/";
    }

    public static void displayToast(Context context, String text, Toast toast) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void displayToast(Context context, String text, Toast toast, int gravity) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }
}
