package com.android.example.record.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.example.record.databinding.BaidumapRgcItemBinding;
import com.baidu.mapapi.search.core.PoiInfo;

import java.util.List;

public class PoiItemAdapter extends RecyclerView.Adapter<PoiItemAdapter.ViewHolder>  {

    public List<PoiInfo> poiInfos; // poi信息
    private int curSelectPos = 0;  // 当前选中的item pos

    private OnItemClickListener onItemClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final BaidumapRgcItemBinding binding;

        public ViewHolder(@NonNull BaidumapRgcItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }
    }

    public PoiItemAdapter(List<PoiInfo> poiInfoList) {
        poiInfos = poiInfoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        BaidumapRgcItemBinding binding = BaidumapRgcItemBinding.inflate( LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);

        holder.binding.getRoot().setOnClickListener(v -> {
            if (null == onItemClickListener) {
                return;
            }

            int pos = holder.getBindingAdapterPosition();
            if (pos == curSelectPos) {
                return;
            }

            curSelectPos = pos;

            notifyDataSetChanged();

            if (null != poiInfos && pos < poiInfos.size()) {
                PoiInfo poiInfo = poiInfos.get(pos);
                onItemClickListener.onItemClick(pos, poiInfo);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position < 0) {
            return;
        }

        modifyItemSelectState(holder, position);

        if (null == poiInfos || position >= poiInfos.size()) {
            return;
        }

        if (0 == position) {
            PoiInfo poiInfo = poiInfos.get(0);
            bindAddressInfo(holder, poiInfo);
        } else {
            bindPoiInfo(holder, position);
        }

    }

    /**
     * 由于Recyclerview的ViewHolder复用逻辑，导致滑动后，下一个绑定的item复用ViewHolder，会复用前一个item的选中状态
     * 这里需要对item的选中状态做下修正
     */
    private void modifyItemSelectState(ViewHolder viewHolder, int position) {
        if (position != curSelectPos) {
            viewHolder.binding.imgCheck.setVisibility(View.GONE);
        } else {
            viewHolder.binding.imgCheck.setVisibility(View.VISIBLE);
            onItemClickListener.onItemCertain(position);
        }
    }

    private void bindPoiInfo(ViewHolder viewHolder, int position) {
        PoiInfo poiInfo = poiInfos.get(position);
        if (null == poiInfo) {
            return;
        }

        String name = poiInfo.getName();
        
        viewHolder.binding.poiResultName.setText(name);
        viewHolder.binding.poiResultDetail.setText(poiInfo.getAddress());
        String poiAddress = poiInfo.getAddress();
        if (!TextUtils.isEmpty(poiAddress)) {
            if (View.INVISIBLE == viewHolder.binding.poiResultDetail.getVisibility()
                    || View.GONE == viewHolder.binding.poiResultDetail.getVisibility()) {
                androidx.appcompat.widget.LinearLayoutCompat.LayoutParams layoutParams =
                        new androidx.appcompat.widget.LinearLayoutCompat.LayoutParams(viewHolder.binding.poiResultName.getLayoutParams());
                layoutParams.setMargins(0, 40, 0, 0);
                viewHolder.binding.poiResultName.setLayoutParams(layoutParams);
                viewHolder.binding.poiResultDetail.setVisibility(View.VISIBLE);
                viewHolder.binding.poiResultDetail.setText(poiAddress);
            }
            viewHolder.binding.poiResultDetail.setText(poiAddress);
        } else {
            hideAddressInfo(viewHolder);
        }
    }

    private void bindAddressInfo(ViewHolder viewHolder, PoiInfo poiInfo) {
        String name = "【" + poiInfo.getAddress() + "】";
        viewHolder.binding.poiResultName.setText(name);
        hideAddressInfo(viewHolder);
    }

    private void hideAddressInfo(ViewHolder viewHolder) {
        androidx.appcompat.widget.LinearLayoutCompat.LayoutParams layoutParams =
                new androidx.appcompat.widget.LinearLayoutCompat.LayoutParams(viewHolder.binding.poiResultName.getLayoutParams());
        layoutParams.setMargins(0, 40, 0, 40);
        viewHolder.binding.poiResultName.setLayoutParams(layoutParams);
        viewHolder.binding.poiResultDetail.setVisibility(View.GONE);
    }

    /**
     * 获取item数目
     */
    @Override
    public int getItemCount() {
        if (null != poiInfos) {
            return poiInfos.size();
        }

        return 0;
    }

    /**
     * 定义item点击回调接口
     */
    public interface OnItemClickListener {
        void onItemClick(int position, PoiInfo poiInfo);
        void onItemCertain(int position);
    }

    public void updateData(List<PoiInfo> poiInfos) {
        this.poiInfos = poiInfos;
        notifyDataSetChanged();
        curSelectPos = 0;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
