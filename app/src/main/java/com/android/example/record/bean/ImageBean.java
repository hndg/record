package com.android.example.record.bean;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import androidx.appcompat.widget.AppCompatImageView;

public class ImageBean {
    final AppCompatImageView imageView;
    String saveName;
    String clock;

    public ImageBean(AppCompatImageView imageView) {
        this.imageView = imageView;
    }

    public String getSaveName() {
        if (saveName != null) {
            return saveName;
        }

        return "";
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    public String getClock() {
        return clock;
    }

    public void setClock(String clock) {
        this.clock = clock;
    }

    public Bitmap getBitmap() {
        return ((BitmapDrawable) imageView.getDrawable()).getBitmap();
    }
}
