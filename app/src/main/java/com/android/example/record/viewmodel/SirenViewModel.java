package com.android.example.record.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SirenViewModel extends ViewModel {
    private MutableLiveData<String> siren;

    public MutableLiveData<String> getSiren() {
        if (siren == null) {
            siren = new MutableLiveData<>();
        }

        return siren;
    }
}