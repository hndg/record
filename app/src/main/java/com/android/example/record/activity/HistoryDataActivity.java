package com.android.example.record.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.android.example.record.MainActivity;
import com.android.example.record.R;
import com.android.example.record.adapter.HistoryDataAdapter;
import com.android.example.record.bean.HistoryData;
import com.android.example.record.databinding.ActivityHistoryDataBinding;
import com.android.example.record.utils.PopupDataUtil;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

public class HistoryDataActivity extends AppCompatActivity {

    private ActivityHistoryDataBinding binding;

    private List<HistoryData> historyDataList = new ArrayList<>();
    private HistoryDataAdapter historyDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHistoryDataBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        initView();

        initData();

        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        binding.subRecyclerView.setLayoutManager(layoutManager);
        binding.subRecyclerView.setAdapter(historyDataAdapter);
        historyDataAdapter.setOnItemClickListener(itemClickListener);
        binding.swipeRefresh.setOnRefreshListener(this::refreshData);
    }

    private final HistoryDataAdapter.OnItemClickListener itemClickListener = new HistoryDataAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, HistoryDataAdapter.ViewName viewName, int position) {
            if (view.getId() == R.id.options) {
                PopupDataUtil.showPopupWindow(position, HistoryDataActivity.this, historyDataList);
            }
        }

        @Override
        public void onItemLongClick(View view) {

        }
    };

    private void refreshData() {
        new Thread(() -> {
            binding.subRecyclerView.removeAllViews();
            runOnUiThread(() -> {
                initData();
                historyDataAdapter.notifyItemRangeChanged(0, historyDataList.size());
                binding.subRecyclerView.removeAllViews();
                binding.swipeRefresh.setRefreshing(false);
            });
        }).start();
    }

    private void initData() {
        historyDataList = LitePal.findAll(HistoryData.class);
        historyDataAdapter = new HistoryDataAdapter(historyDataList);
    }


    private void initView() {
        setSupportActionBar(binding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.search) {
            Intent intent = new Intent(this, QueryDataActivity.class);
            startActivity(intent);
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.query, menu);
        return true;
    }
}