package com.android.example.record.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.android.example.record.R;
import com.android.example.record.bean.HistoryData;
import com.android.example.record.databinding.ActivityDetailsBinding;
import com.android.example.record.utils.OkHttpUtils;
import com.android.example.record.utils.PopupDataUtil;
import com.android.example.record.utils.Utils;
import com.bumptech.glide.Glide;

import org.litepal.LitePal;

import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private ActivityDetailsBinding binding;
    private String img_path;
    private int id;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        img_path = Utils.getImgPath(this);
        Intent intent = getIntent();
        id = intent.getIntExtra("ID", -1);
        if (id == -1) {
            Utils.displayToast(DetailsActivity.this, "获取数据失败", toast);
            finish();
            return;
        }

        List<HistoryData> historyDataList = LitePal.findAll(HistoryData.class, id);
        for (int i = 0; i < historyDataList.size(); i++) {
            showDataById(historyDataList.get(i));
        }
    }

    private void showDataById(HistoryData historyData) {
        binding.saveTime.setText(String.format("%s %s %s", historyData.getSave_date(), historyData.getSave_week(), historyData.getSave_time()));
        binding.siren.setText(historyData.getSiren());
        binding.number.setText(historyData.getNumber());
        binding.color.setText(historyData.getColor());
        binding.address.setText(historyData.getAddress());
        Glide.with(this).load(img_path + historyData.getImg_front_uri())
                .optionalCenterInside().into(binding.imgFront);
        Glide.with(this).load(img_path + historyData.getImg_overall_uri())
                .optionalCenterInside().into(binding.imgOverall);
        Glide.with(this).load(img_path + historyData.getImg_rear_uri())
                .optionalCenterInside().into(binding.imgRear);
        Glide.with(this).load(img_path + historyData.getImg_inside_uri())
                .optionalCenterInside().into(binding.imgInside);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.upload) {
            upload();
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.upload, menu);
        return true;
    }

    private void upload() {
        Utils.displayToast(DetailsActivity.this, "连接服务", toast);
        OkHttpUtils.doGetRequest(Utils.getUploadPictureUri(this), PopupDataUtil.testCallback(getJSON(), this));
    }

    // 初始化上传文件的数据
    private String getJSON() {
        int id = this.id;
        List<HistoryData> historyDataPicList = LitePal.select("*")
                .where("id = ?", String.valueOf(id)).find(HistoryData.class);
        String jsonData = historyDataPicList.get(0).jsonString(this);
        Log.d("data", jsonData);

        return jsonData;
    }
}