package com.android.example.record;

import android.app.Application;
import android.app.Service;
import android.os.Vibrator;

import com.android.example.record.service.LocationService;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;

import org.litepal.LitePal;

public class LocationApplication extends Application {
    public LocationService locationService;
    public Vibrator vibrator;

    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);

         // 初始化定位sdk，建议在Application中创建
        locationService = new LocationService(getApplicationContext());
        vibrator = (Vibrator) getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
        SDKInitializer.initialize(getApplicationContext());
        SDKInitializer.setCoordType(CoordType.BD09LL);
    }
}
