package com.android.example.record.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.example.record.R;
import com.android.example.record.databinding.FragmentSetUploadBinding;
import com.android.example.record.databinding.FragmentSettingBinding;
import com.android.example.record.databinding.ToolbarBinding;
import com.android.example.record.utils.SharedPreferencesUtil;
import com.android.example.record.utils.Utils;

public class SetUploadFragment extends Fragment {

    private FragmentSettingBinding fragmentSettingBinding;
    private FragmentSetUploadBinding binding;

    public SetUploadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentSetUploadBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        ToolbarBinding toolbarBinding = binding.toolbar;

        String protocol = SharedPreferencesUtil.getParam(requireActivity(),
                SharedPreferencesUtil.HTTP,
                Utils.PROTOCOL);
        String upload_uri = SharedPreferencesUtil.getParam(requireActivity(),
                SharedPreferencesUtil.UPLOAD_URI,
                Utils.UPLOAD_PICTURE_URI);

        toolbarBinding.title.setText("地址");

        if (!protocol.equals("https://")) {
            binding.http.setChecked(true);
            binding.https.setChecked(false);
        } else {
            binding.http.setChecked(false);
            binding.https.setChecked(true);
        }

        binding.uploadUri.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        binding.uploadUri.setText(upload_uri);

        toolbarBinding.sure.setOnClickListener(this::SetUploadUri);

        toolbarBinding.hide.setOnClickListener(this::closeDrawer);

        return view;
    }

    private void SetUploadUri(View view) {
        int selectedProtocolID = binding.protocol.getCheckedRadioButtonId();
        if (selectedProtocolID == R.id.http) {
            SharedPreferencesUtil.setParam(requireActivity(), SharedPreferencesUtil.HTTP, Utils.PROTOCOL);
        } else if (selectedProtocolID == R.id.https) {
            SharedPreferencesUtil.setParam(requireActivity(), SharedPreferencesUtil.HTTP, "https://");
        }

        Editable uri = binding.uploadUri.getText();
        if (!TextUtils.isEmpty(uri)) {
            SharedPreferencesUtil.setParam(requireActivity(), SharedPreferencesUtil.UPLOAD_URI, uri.toString());
        } else {
            SharedPreferencesUtil.setParam(requireActivity(), SharedPreferencesUtil.UPLOAD_URI, Utils.UPLOAD_PICTURE_URI);
        }
        fragmentSettingBinding.settingsDrawerLayout.closeDrawer(fragmentSettingBinding.settingsViewPager2);
    }

    private void closeDrawer(View view) {
        fragmentSettingBinding.settingsDrawerLayout.closeDrawer(fragmentSettingBinding.settingsViewPager2);
    }

    @Override
    public void onStart() {
        super.onStart();
        fragmentSettingBinding = SettingFragment.binding;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}